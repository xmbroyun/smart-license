/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseTest.java
 * Date: 2020-03-22
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.client;

import org.smartboot.license.client.common.LicenseConfig;

import java.io.File;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/21
 */
public class LicenseTest {
    public static void main(String[] args) throws Exception {
        File file=new File("/Users/zhengjunwei/IdeaProjects/smart-license/license-client/src/test/resources/license.txt");
        License license = new License();
        LicenseConfig licenseData=license.loadLicense(file);
        System.out.println(licenseData.getOriginal());
    }
}
